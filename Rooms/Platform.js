import { Goal } from "../Objects/Goal.js";
import { Block } from "../Objects/Block.js";
import { Level } from "../GameFrame/Level.js";
import { Player } from "../Objects/Player.js";
import { Banner } from "../Objects/Banner.js";
import { Monster } from "../Objects/Monster.js";
import { Monster2 } from "../Objects/Monster2.js";
import { TextObject } from "../GameFrame/TextObject.js";
import { Globals } from "../GameFrame/Globals.js";


export class Platform extends Level {

    constructor(room_loop, canvas, ctx){
        super(room_loop, canvas, ctx);

        this.set_background_image("wood_background");

        Globals.LIVES = 3;
        this.time = 0;

        let room_objects = [
            'lmmmmmmmmmmmmmmmmmmmmmmmr',
            'ug______________________u',
            'u_______________________u',
            'umr___G____________M____u',
            'u_______________________u',
            'u_lmmr_lmmmmmr______lr__u',
            'u_____________lmmmr_____u',
            'u______________________lu',
            'u__________M____________u',
            'u__________________Glmmmu',
            'u_____M________lr_______u',
            'u_________lmmr___lr_____u',
            'u_______________________u',
            'ummmr__lr_______________u',
            'u________lmmmr__________u',
            'up______________________u',
            'ummmmmmmmmmmmmmmmmmmmmmmu'
        ];

        for(let i = 0; i < room_objects.length; i++){
            for(let j = 0; j < room_objects[i].length; j++){
                let obj = room_objects[i].charAt(j);
                switch(obj){
                    case 'm':
                        this.add_room_object(new Block(this, j * 32, i * 32, "Grass_Tile_Flat"));
                        break;
                    case 'l':
                        this.add_room_object(new Block(this, j * 32, i * 32, "Grass_Tile_Corner_Edge_l"));
                        break;
                    case 'r':
                        this.add_room_object(new Block(this, j * 32, i * 32, "Grass_Tile_Corner_Edge_r"));
                        break;
                    case 'u':
                        this.add_room_object(new Block(this, j * 32, i * 32, "Grass_Tile_lower"));
                        break;
                    case 'p':
                        this.add_room_object(new Player(this, j * 32 + 2, i * 32 + 3));
                        break;
                     case 'g':
                         this.add_room_object(new Goal(this, j * 32 + 2, i * 32 + 2));
                         break;
                    case 'G':
                        this.add_room_object(new Monster2(this, j * 32 + 2, i * 32 + 2));
                        break;
                    case 'M':
                        this.add_room_object(new Monster(this, j * 32 + 2, i * 32 + 2));
                        break;
                }
            }
        }

        // Add Banner for game info (lives) 800 x 56
        this.add_room_object(new Banner(this, 0, 544));

        // Add Lives Text
        this.score_text = new TextObject(this, 20, 594, `Lives: ${Globals.LIVES}`);
        this.score_text.depth = 1000;
        this.score_text.colour = "white";
        this.add_room_object(this.score_text)

        // Add Timer Text
        this.time_text = new TextObject(this, 400, 594, `Time: ${this.time}`);
        this.time_text.depth = 1000;
        this.time_text.colour = "white";
        this.add_room_object(this.time_text)

        this.set_timer(30, this.update_time);
    }

    update_lives(value){
        Globals.LIVES += value;
        if(Globals.LIVES == 0){
            this.running = false;
        }
        this.score_text.text = `Lives: ${Globals.LIVES}`;
    }

    update_time(){
        this.time += 1;
        this.time_text.text = `Time: ${this.time}`;
        this.set_timer(30, this.update_time);
    }
}
