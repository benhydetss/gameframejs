import { Globals } from "../GameFrame/Globals.js";
import { Level } from "../GameFrame/Level.js";
import { Carrot } from "../Objects/Carrot.js";
import { Bomb } from "../Objects/Bomb.js";

export class VegieRoom extends Level {

    constructor(room_loop, canvas, ctx){
        super(room_loop, canvas, ctx);

        this.set_background_image("background");

        let carrot_obj_1 = new Carrot(this, 200, 50);
        carrot_obj_1.set_direction(Globals.randomIntRange(0, 359), 8);
        this.add_room_object(carrot_obj_1);

        let carrot_obj_2 = new Carrot(this, 300, 150);
        carrot_obj_2.set_direction(Globals.randomIntRange(0, 359), 8);
        this.add_room_object(carrot_obj_2);

        let carrot_obj_3 = new Carrot(this, 400, 250);
        carrot_obj_3.set_direction(Globals.randomIntRange(0, 359), 8);
        this.add_room_object(carrot_obj_3);

        let carrot_obj_4 = new Carrot(this, 500, 350);
        carrot_obj_4.set_direction(Globals.randomIntRange(0, 359), 8);
        this.add_room_object(carrot_obj_4);

        let carrot_obj_5 = new Carrot(this, 600, 450);
        carrot_obj_5.set_direction(Globals.randomIntRange(0, 359), 8);
        this.add_room_object(carrot_obj_5);

        let carrot_obj_6 = new Carrot(this, 700, 500);
        carrot_obj_6.set_direction(Globals.randomIntRange(0, 359), 8);
        this.add_room_object(carrot_obj_6);
        
        let bomb_obj_1 = new Bomb(this, 200, 350);
        this.add_room_object(bomb_obj_1);
        let bomb_obj_2 = new Bomb(this, 300, 250);
        this.add_room_object(bomb_obj_2);
        let bomb_obj_3 = new Bomb(this, 400, 150);
        this.add_room_object(bomb_obj_3);
        let bomb_obj_4 = new Bomb(this, 500, 50);
        this.add_room_object(bomb_obj_4);

        bomb_obj_1.set_direction(Globals.randomIntRange(0, 359), 8);
        bomb_obj_2.set_direction(Globals.randomIntRange(0, 359), 8);
        bomb_obj_3.set_direction(Globals.randomIntRange(0, 359), 8);
        bomb_obj_4.set_direction(Globals.randomIntRange(0, 359), 8);
    }
}
