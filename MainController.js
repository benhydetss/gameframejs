import { Globals } from "./GameFrame/Globals.js";

function room_loop(canvas, ctx){
    var levels = Globals.levels;
    var curr_level = Globals.next_level;
    Globals.next_level += 1;
    Globals.next_level %= levels.length;
    var class_name = levels[curr_level];
    var mod_name = `./Rooms/${class_name}.js`;

    import(mod_name).then(module => {
        const LoadedClass = module[class_name];
        const new_object = new LoadedClass(room_loop, canvas, ctx);
        new_object.run(room_loop);
        canvas.addEventListener('click', function(e){
            new_object.click_event = true;
            new_object.click_x = e['offsetX'];
            new_object.click_y = e['offsetY'];
        });
        window.addEventListener('keydown', function(e) {
            new_object.key_down(e["code"]);
        });
        window.addEventListener('keyup', function(e) {
            new_object.key_up(e["code"]);
        });
    });
}

document.addEventListener("DOMContentLoaded", function loader(){

    var canvas = document.getElementById("mainwin");
    var ctx = canvas.getContext("2d");

    document.getElementById("heading").innerHTML = Globals.window_name;

    var mainwin = document.getElementById("mainwin");
    mainwin.setAttribute("width", Globals.SCREEN_WIDTH);
    mainwin.setAttribute("height", Globals.SCREEN_HEIGHT);
    mainwin.setAttribute("style", `width:${Globals.SCREEN_WIDTH}px; height:${Globals.SCREEN_HEIGHT}px;`);

    // Download all project images
    for(let image_name of Globals.images_list){
        const new_elem = new Image(46, 64);
        new_elem.src = "./Images/" + image_name[0] + image_name[1];
        new_elem.setAttribute("id", image_name[0]);
        document.body.appendChild(new_elem);
        document.getElementById(image_name[0]).style.visibility = "hidden";
    }

    // Download all project sound files
    for(let sound_name of Globals.sounds_list){
        const new_elem = new Audio();
        new_elem.src = "./Sounds/" + sound_name[0] + sound_name[1];
        new_elem.setAttribute("id", sound_name[0]);
        document.body.appendChild(new_elem);
    }

    Globals.next_level = Globals.start_level;
    room_loop(canvas, ctx);
});
