# GameFrameJS

GameFrameJS is a JavaScript port of the Python Framework "GameFrame". GameFrameJS aims to help with learning the concepts of text based game programming in JavaScript,  without getting caught up in the implementation details.

GameFrameJS is set up as an event driven framework. Programmers define Rooms and Room Objects, then write functions to handle certain events such as collisions, button clicks and so on. Just define all the items of your game, then let it run. GameFrameJS handles the Game loop and collision detection, just register your object for an event and write the code that will run when that event occurs.

GameFrameJS was primarily written for education, however it can be used to make a variety of games that can be can be freely shared, altered and improved. It’s free and available for everyone to use, students, hobbyist and accomplished programmers alike.
