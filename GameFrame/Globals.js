
export class Globals {

    static running = true;

    static FRAMES_PER_SECOND = 30;

    static SCREEN_WIDTH = 800;
    static SCREEN_HEIGHT = 600;

    // Set the Window display name
    static window_name = "Vegie Samurai and Joe's Escape";

    // Set the order of the rooms
    static levels = ["VegieRoom", "Platform"];

    // Set the starting level
    static start_level = 0;

    // Set this number to the level you want to jump to when the game ends
    static end_game_level = 4;

    // This variable keeps track of the room that will follow the current room
    // Change this value to move through rooms in a non-sequential manner
    static next_level = 0;

    // List of all images in the Images folder
    static images_list = [
        ["carrot", ".png"],
        ["background", ".jpg"],
        ["bomb", ".png"],
        ["wood_background", ".jpg"],
        ["Grass_Tile_Flat", ".png"],
        ["Grass_Tile_Corner_Edge_l", ".png"],
        ["Grass_Tile_Corner_Edge_r", ".png"],
        ["Grass_Tile_lower", ".png"],
        ["player", ".png"],
        ["player_left", ".png"],
        ["player_right", ".png"],
        ["monster", ".png"],
        ["goal", ".png"],
        ["banner", ".png"]
    ];

    // List of all sounds in the Sounds folder
    static sounds_list = [
        ["success", ".ogg"],
        ["doh", ".wav"]
    ]
    
    // Used for the room creation loop
    static room_running = false;

    // Helper function to generate random integer
    // within a given range
    static randomIntRange(min, max){
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    /*************************************************
     * User Defined Global Variables below this line *
     *************************************************/

    // Set the initial score
    static SCORE = 0;

    // Set the starting number of lives
    static LIVES = 3;
}
