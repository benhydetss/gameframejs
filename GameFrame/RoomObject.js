import { Globals } from "./Globals.js";


export class RoomObject {

    constructor(room, x, y){

        this.room = room;
        this.depth = 0;
        this.x = x;
        this.y = y;
        this.prev_x = x;
        this.prev_y = y;
        this.width = 0;
        this.height = 0;
        this.x_speed = 0;
        this.y_speed = 0;
        this.gravity = 0;
        this.image = 0;
        this.handle_mouse_events = false;
        this.handle_key_events = false;

        this.collision_object_types = new Set();
        this.collision_objects = [];
    }

    draw_to_canvas(ctx){
        ctx.drawImage(this.image, this.x, this.y, this.width, this.height);
    }

    load_image(file_name){
        return document.getElementById(file_name);
    }

    set_image(imagesrc, width, height) {
        this.image = imagesrc;
        this.width = width;
        this.height = height;
    }

    register_collision_object(collision_object){
        this.collision_object_types.add(collision_object);
    }

    update() {
        this.y_speed = this.y_speed + this.gravity;
        this.x += this.x_speed;
        this.y += this.y_speed;
    }

    prestep(){
        return;
    }

    step() {
        return;
    }

    check_collisions() {
        for(let item of this.collision_objects){
            if(this.rect_colliderect(item)){
                let item_type = item.constructor.name;
                this.handle_collision(item, item_type);
            }
        }
    }

    collides_at(x, y, collision_type){
        let test_x = this.x + x;
        let test_y = this.y + y;
        let collision_found = false;
        for(let item of this.collision_objects){
            if(item.constructor.name == collision_type) {
                if(this.point_colliderect(test_x, test_y, this.width, this.height, item)){
                    collision_found = true;
                    break;
                }
            }
        }
        return collision_found
    }

    handle_collision(other, other_type){
        return;
    }

    clicked() {
        return;
    }

    delete_object(obj){
        this.room.delete_object(obj);
    }

    remove_object(obj){
        for(let i = this.collision_objects.length - 1; i >=0; i--) {
            if(this.collision_objects[i] === obj){
                this.collision_objects.splice(i, 1);
            }
        }
    }

    bounce(other){
        // this is to the side of other
        if(other.rect_top() < this.rect_centery < other.rect_bottom()){
            this.x_speed *= -1;
            this.x = this.prev_x;
        }

        // this is above or below other
        if(other.rect_left() < this.rect_centerx() < other.rect_right()){
            this.y_speed *= -1;
            this.y = this.prev_y;
        }
    }

    blocked() {
        this.x = this.prev_x;
        this.y = this.prev_y;
        this.x_speed = 0;
        this.y_speed = 0;
    }

    set_timer(ticks, function_call){
        this.room.add_timer(ticks, this, function_call.bind(this));
    }

    set_direction(angle, speed){

        let xandy = this.get_direction(angle, speed);
        if(angle == 0){
            this.x_speed = speed;
            this.y_speed = 0;
        } else if(angle < 90){
            this.x_speed = xandy[0];
            this.y_speed = xandy[1];
        } else if(angle == 90){
            this.x_speed = 0;
            this.y_speed = speed;
        } else if(angle < 180){
            this.x_speed = -xandy[1];
            this.y_speed = xandy[0];
        } else if(angle == 180){
            this.x_speed = -speed;
            this.y_speed = 0;
        } else if(angle < 270){
            this.x_speed = -xandy[0];
            this.y_speed = -xandy[1];
        } else if(angle == 270){
            this.x_speed = 0;
            this.y_speed = -speed;
        } else if(angle < 360){
            this.x_speed = xandy[1];
            this.y_speed = xandy[0];
        }
    }

    get_direction(angle, speed){
        let new_x_speed = Math.round(Math.cos(angle * (Math.PI / 180)) * speed);
        let new_y_speed = Math.round(Math.sin(angle * (Math.PI / 180)) * speed);
        return [new_x_speed, new_y_speed];
    }

    rect_left() {
        return this.x;
    }

    rect_right() {
        return this.x + this.width;
    }

    rect_top() {
        return this.y;
    }

    rect_bottom() {
        return this.y + this.height;
    }

    rect_centery() {
        return this.y + this.height / 2;
    }

    rect_centerx() {
        return this.x + this.width / 2;
    }

    rect_colliderect(other){
        let x_overlap = (
                this.x <= other.rect_right() && this.x >= other.x
            ) || (
                this.rect_right() <= other.rect_right() && this.rect_right() >= other.x
            );
        
        let y_overlap = (
                this.y <= other.rect_bottom() && this.y >= other.y
            ) || (
                this.rect_bottom() <= other.rect_bottom() && this.rect_bottom() >= other.y
            );
        
        return x_overlap && y_overlap;
    }

    point_colliderect(x, y, width, height, other) {
        let x_overlap = (
                x <= other.rect_right() && x >= other.x
            ) || (
                (x + width) <= other.rect_right() && (x + width) >= other.x
            );
    
        let y_overlap = (
            y <= other.rect_bottom() && y >= other.y
            ) || (
                (y + height) <= other.rect_bottom() && (y + height) >= other.y
            );
    
        return x_overlap && y_overlap;
    }

    collidepoint(x, y) {
        if(x >= this.x && x <= this.rect_right() && y >= this.y && y <= this.rect_bottom()){
            return true;
        } else {
            return false;
        }
    }
}
