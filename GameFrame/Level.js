import { Globals } from "./Globals.js";


export class Level {
    
    constructor(room_loop, canvas, ctx){
        this.room_loop = room_loop;
        this.canvas = canvas;
        this.ctx = ctx;
        this.objects = [];
        this.keyboard_objects = [];
        this.mouse_objects = [];
        this.running = false;
        this.quitting = false;
        this.background_color = [0, 0, 0];
        this.background_set = false;
        this.background_image = 0;
        this.background_y = 0;
        this.background_scroll_speed = 0;
        this.background_scrolling = false;
        this.user_events = [];

        this.fps = Globals.FRAMES_PER_SECOND;
        this.timeout_count = 0;
        this.color_count = 0;

        this.ctx.rect(0, 0, Globals.SCREEN_WIDTH, Globals.SCREEN_HEIGHT);
        this.ctx.fillStyle = `rgb(${this.background_color[0]}, ${this.background_color[1]} , ${this.background_color[2]})`;
        this.ctx.fill();

        // Set up Click vars
        this.click_event = false;
        this.click_x = 0;
        this.click_y = 0;

        // Set up key event structure
        this.keys_pressed = new Object();
        this.keys_activated = 0;
    }

    run(room_loop){
        this.running = true;

        // Initialise the collision lists
        for(let obj of this.objects){
            this.init_collision_list(obj);
        }
        Globals.room_running = true;
        this.draw_loop(room_loop);
    }

    draw_loop(room_loop){

        // Set event for next frame
        if(this.running){
            setTimeout(() => {
                requestAnimationFrame(() => this.draw_loop());
            }, 1000 / this.fps);
        } else {
            this.room_loop(this.canvas, this.ctx);
        }

        // Update previous coordinates
        for(let obj of this.objects){
            obj.prev_x = obj.x;
            obj.prev_y = obj.y;
        }

        // Process user events (timers)
        this.process_user_events();

        // Call prestep on all objects
        for (let item of this.objects){
            item.prestep();
        }

        if(this.click_event){
            this.click_event = false;
            for(let obj of this.mouse_objects){
                if(obj.collidepoint(this.click_x, this.click_y)){
                    obj.clicked();
                }
            }
        }
        if(this.keys_activated > 0){
            for(let obj of this.keyboard_objects){
                obj.key_pressed(this.keys_pressed);
            }
        }
        
        // Call update on all objects
        for (let item of this.objects){
            // draw item
            item.update();
            item.step();
        }

        // Check collisions
        for(let item of this.objects){
            item.check_collisions();
        }

        // Clear the canvas
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.fill();
        // Draw background if set
        if(this.background_set) {
            this.ctx.drawImage(this.background_image, 0, 0, Globals.SCREEN_WIDTH, Globals.SCREEN_HEIGHT);
        }

        // Tell each object to Draw itself on to the canvas
        for(let item of this.objects) {
            item.draw_to_canvas(this.ctx);
            // this.ctx.drawImage(item.image, item.x, item.y, item.width, item.height);
        }
    }

    key_down(key){
        if(!(key in this.keys_pressed) || !this.keys_pressed[key]){
            this.keys_pressed[key] = true;
            this.keys_activated += 1;
        }
    }

    key_up(key){
        this.keys_pressed[key] = false;
        this.keys_activated -= 1;
    }
    set_background_image(image){
        this.background_set = true;
        this.background_image = document.getElementById(image);
    }

    add_room_object(item){
        if(this.objects.length == 0){
            this.objects.push(item);
        } else {
            for(let i = 0; i < this.objects.length; i++) {
                if(this.objects[i].depth > item.depth){
                    this.objects.splice(i, 0, item);
                    break;
                } else if(i == this.objects.length - 1){
                    this.objects.push(item);
                    break;
                }
            }
        }
        // Add objects that handle key events to array
        if(item.handle_key_events){
            this.keyboard_objects.push(item);
        }
        // Add objects that handle mouse events to array
        if(item.handle_mouse_events){
            this.mouse_objects.push(item);
        }
        // Update collision this if the object is being added during
        // running of the room
        if(this.running) {
            this.dynamic_init_collision_list(item);
        }
    }

    load_sound(name){
        return document.getElementById(name);
    }

    init_collision_list(room_object){
        for(let obj_name of room_object.collision_object_types){
            for(let obj_instance of this.objects){
                if(obj_instance.constructor.name == obj_name && obj_instance !== room_object){
                    room_object.collision_objects.push(obj_instance);
                }
            }
        }
    }

    dynamic_init_collision_list(room_object){
        this.init_collision_list(room_object);
        let obj_type = room_object.constructor.name;
        for(let obj of this.objects){
            if(obj !== room_object){
                if(obj.collision_object_types.includes(obj_type)){
                    obj.collision_objects.push(room_object);
                }
            }
        }
    }

    delete_object(obj){
        for(let i = this.objects.length - 1; i >= 0; i--) {
            if(this.objects[i] === obj){
                this.objects.splice(i, 1);
            } else {
                this.objects[i].remove_object(obj);
            }
        }
        for(let i = this.keyboard_objects.length -1; i >= 0; i--){
            if(this.keyboard_objects[i] === obj){
                this.keyboard_objects.splice(i, 1);
            }
        }
        for(let i = this.mouse_objects.length -1; i >= 0; i--){
            if(this.mouse_objects[i] === obj){
                this.mouse_objects.splice(i, 1);
            }
        }
        // Remove any timed function calls for the deleted object
        for(let i = this.user_events.length - 1; i >=0; i--){
            if(obj === this.user_events[i][1]){
                this.user_events.splice(i, 1);
            }
        }
    }

    set_timer(ticks, function_call){
        this.add_timer(ticks, this, function_call.bind(this));
    }

    add_timer(ticks, instance, function_call){
        this.user_events.push([ticks, instance, function_call]);
    }

    process_user_events(){
        for(let i = this.user_events.length - 1; i >=0; i--){
            this.user_events[i][0] -= 1;
            if(this.user_events[i][0] <= 0){
                this.user_events[i][2]();
                this.user_events.splice(i, 1);
            }
        }
    }
}
