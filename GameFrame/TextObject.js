import { RoomObject } from "./RoomObject.js";


export class TextObject extends RoomObject {
    constructor(room, x, y, text){
        super(room, x, y);
        
        this.text = text;
        this.size = 60;
        this.font = 'Comic Sans MS';
        this.colour = 'black';
        this.bold = false;
    }

    draw_to_canvas(ctx){
        ctx.textBaseLine = 'top';
        ctx.font = `${this.size}px ${this.font}`;
        ctx.fillStyle = this.colour;
        let text_info = ctx.measureText(this.text);
        this.width = text_info.width;
        this.height = this.size;

        ctx.fillText(this.text, this.x, this.y)
    }
}
