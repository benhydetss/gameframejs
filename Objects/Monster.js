import { RoomObject } from "../GameFrame/RoomObject.js";


export class Monster extends RoomObject {

    constructor(room, x, y){
        super(room, x, y);

        let img = this.load_image("monster");
        this.set_image(img, 28, 25);

        this.y_speed = -6;

        this.register_collision_object("Block");
    }

    handle_collision(other, other_type){
        this.y_speed *= -1;
        this.y = this.prev_y;
    }
}
