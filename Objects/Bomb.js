import { Globals } from "../GameFrame/Globals.js";
import { RoomObject } from "../GameFrame/RoomObject.js";


export class Bomb extends RoomObject {

    constructor(room, x, y){
        super(room, x, y);

        let image = this.load_image("bomb");
        this.set_image(image, 64, 64);

        this.register_collision_object("Bomb");
        this.register_collision_object("Carrot");

        this.handle_mouse_events = true;

        this.clicked_sound = this.room.load_sound("doh");

    }

    step() {
        if(this.x >= Globals.SCREEN_WIDTH - this.width  || this.x <= 0) {
            this.x_speed *= -1;
            this.x = this.prev_x;
        }

        if(this.y >= Globals.SCREEN_HEIGHT - this.height || this.y <= 0) {
            this.y_speed *= -1;
            this.y = this.prev_y;
        }
    }

    handle_collision(other, other_type){
        this.bounce(other);
    }

    clicked() {
        this.clicked_sound.play();
        Globals.next_level = 0;
        Globals.SCORE = 0;
        this.room.running = false;
    }
}
