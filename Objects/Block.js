import { RoomObject } from "../GameFrame/RoomObject.js";


export class Block extends RoomObject {

    constructor(room, x, y, image){
        super(room, x, y);

        let img = this.load_image(image);
        this.set_image(img, 32, 32);
    }
}
