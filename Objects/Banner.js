import { RoomObject } from "../GameFrame/RoomObject.js";


export class Banner extends RoomObject {

    constructor(room, x, y){
        super(room, x, y);

        let img = this.load_image("banner");
        this.set_image(img, 800, 56);
    }
}
