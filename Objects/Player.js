import { RoomObject } from "../GameFrame/RoomObject.js";


export class Player extends RoomObject {

    constructor(room, x, y){
        super(room, x, y);

        let player = this.load_image('player');
        this.player_left = this.load_image('player_left')
        this.player_right = this.load_image('player_right')
        this.set_image(player, 25, 32);
        this.depth = 5;

        this.gravity = 0;

        this.handle_key_events = true;

        // Register the objects with which
        // this object handles collisions
        this.register_collision_object('Block');
        this.register_collision_object('Goal');
        this.register_collision_object('Monster');
        this.register_collision_object('Monster2');

        this.jump_sound = this.room.load_sound("success");
        this.ghost_hit = this.room.load_sound("doh");
    }

    handle_collision(other, other_type){
        if(other_type == 'Block'){
            this.blocked();
            if(this.rect_centery() < other.rect_top()){
                this.y_speed = 0;
                this.gravity = 0;
                this.y = other.y - this.height - 2;
            }
        } else if(other_type == 'Goal'){
            this.room.running = false;
        } else if(other_type == 'Monster' || other_type == 'Monster2'){
            this.ghost_hit.play();
            this.x = 64;
            this.y = 15*32;
            this.room.update_lives(-1);
        }
    }

    key_pressed(keys){
        if(keys["ArrowRight"]){
            this.move_right();
        } else if(keys["ArrowLeft"]){
            this.move_left();
        }
        if(keys["Space"]){
            this.jump();
        }
    }

    move_right(){
        this.set_image(this.player_right, 25, 32);
        this.x += 4;
        if(!this.collides_at(0, 4, 'Block')){
            this.gravity = 1;
        }
    }

    move_left(){
        this.set_image(this.player_left, 25, 32);
        this.x -= 4;
        if(!this.collides_at(0, 4, 'Block')){
            this.gravity = 1;
        }
    }
    
    jump(){
        this.jump_sound.play();
        if(this.collides_at(0, 4, 'Block')){
            this.y_speed = -15;
            this.gravity = 1;
        }
    }
}
