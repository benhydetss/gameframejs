import { RoomObject } from "../GameFrame/RoomObject.js";


export class Goal extends RoomObject {

    constructor(room, x, y){
        super(room, x, y);

        let img = this.load_image("goal");
        this.set_image(img, 28, 25);
    }
}
